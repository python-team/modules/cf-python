.. currentmodule:: cf
.. default-role:: obj

cf.Datetime
===========

.. autoclass:: cf.Datetime
   :no-members:
   :no-inherited-members:

Datetime methods
----------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Datetime.copy	
   ~cf.Datetime.inspect
   ~cf.Datetime.timetuple
   
Datetime class methods
----------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Datetime.utcnow
   