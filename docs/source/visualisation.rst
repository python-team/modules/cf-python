Visualisation
=============

The `cfplot package
<http://climate.ncas.ac.uk/~andy/cfplot_sphinx/_build/html/>`_ at
version 1.7.5 or newer provides metadata-aware visualisation for
cf-python fields. This is a seperate library and `cf` does not depend
on it.
